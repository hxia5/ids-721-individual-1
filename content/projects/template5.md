+++
title = "IDS 721 Week 6 Mini Project"

[extra]
image = "https://upload.wikimedia.org/wikipedia/commons/3/35/GitLab_icon.svg"
link = "https://gitlab.com/hxia5/ids-721-week-6"
technologies = ["AWS", "Rust", "Lambda", "X-Ray tracing", "CloudWatch"]
+++
