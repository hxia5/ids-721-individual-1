+++
title = "IDS 721 Individual Project 1"

[extra]
image = "https://upload.wikimedia.org/wikipedia/commons/3/35/GitLab_icon.svg"
link = "https://gitlab.com/hxia5/ids-721-individual-1"
technologies = ["zola", "Rust", "CSS", "Vercel"]
+++