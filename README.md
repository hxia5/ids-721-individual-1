# IDS 721 Individual Project 1 
[![pipeline status](https://gitlab.com/hxia5/ids-721-individual-1/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-individual-1/-/commits/main)

## Link to Website
https://ids-721-individual-1.vercel.app/

## Screenshot of Home Page

![Alt text](home.png)

## Screenshot of Portfolio Page

![alt text](project.png)

## Video
https://youtu.be/3ViCX4DpUFE

## Overview
* This is my repository of IDS 721 Individual Project 1 - Create a Static Website with Zola.

## Purpose
- Website built with Zola, Hugo, Gatsby, Next.js or equivalent
- GitLab workflow to build and deploy site on push
- Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.


## Key Steps
1. `git init` to create a new repository locally
2. Install Zola using `brew install zola`
3. `zola init my_site` to create a new Zola site
4. Choose a template of the theme from the Zola theme, and deploy it by using `cd themes` and then `git clone <theme repository URL>`. I chose the Hephaestus theme. 
5. After this, follow the instructions to customize your web pages. To fulfill the requirements, I added two sections: "Education" and "Projects". In the "Education" section, I added my undergraduate and graduate university with their badges and web links. In the "Projects" section, I added the links to my Gitlab repositories for the mini projects in IDS 721.
6. Create a `.gitmodules` file in the root directory of the repository with the following content:
```
[submodule "themes/hephaestus"]
	path = themes/hephaestus
	url = https://github.com/BConquest/hephaestus.git
```

7. Sign up for Vercel

8. Install `Vercel CLI`

9. Run `vercel login` to log in to Vercel

10. Run `vercel link` to link the project to Vercel, put `.vercel` in `.gitignore`

![alt text](link.png)

11. Create your `Vercel_TOKEN` in the settings of the Vercel account, find your `VERCEL_ORG_ID` and `VERCEL_PROJECT_ID` in the generated `.vercel` folder, copy them and then add them to the GitLab repository as a secret variable.

12. Set up the GitLab CI/CD pipeline to build the site and deploy it using Vercel:
```
default:
  image: node:18.12.1 

variables:
  GIT_SUBMODULE_STRATEGY: "recursive"
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

deploy_preview:
  stage: deploy
  except:
    - main
  script:
    - npm install --global vercel
    - vercel pull --yes --environment=preview --token=$VERCEL_TOKEN
    - vercel build --token=$VERCEL_TOKEN
    - vercel deploy --prebuilt  --token=$VERCEL_TOKEN

deploy_production:
  stage: deploy
  only:
    - main
  script:
    - npm install --global vercel
    - vercel pull --yes --environment=production --token=$VERCEL_TOKEN
    - vercel build --prod --token=$VERCEL_TOKEN
    - vercel deploy --prebuilt --prod --token=$VERCEL_TOKEN
```

13. Create a repository on GitLab, pull first and then push the local repository to the remote repository.
```
git pull origin main
git add .
git commit -m "Initial commit"
git push origin main
```
14. On your Vercel homepage, click into your project, and then click the "Settings" tab. On the left sidebar, click "Domains", and you will find the domain of your website.
15. On the left sidebar of GitLab, navigate to **settings -> cicd -> general pipeline expand -> pipeline status** to find the passed badge.

References:

1. https://www.getzola.org/documentation/getting-started/installation/
2. https://vercel.com/guides/how-can-i-use-gitlab-pipelines-with-vercel#configuring-gitlab-ci/cd-for-vercel
3. https://www.getzola.org/documentation/deployment/vercel/

